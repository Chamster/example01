﻿using Api.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Api
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<Context>(_ => _.UseInMemoryDatabase("donkey"));

      services.AddMvc()
        .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
        .AddJsonOptions(_ =>
        {
          _.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
          _.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        });
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment environment)
    {
      if (environment.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        using (var serviceScope = app.ApplicationServices
          .GetRequiredService<IServiceScopeFactory>().CreateScope())
          Seed(serviceScope.ServiceProvider.GetService<Context>());
      }
      else
        app.UseHsts();

      app.UseHttpsRedirection();
      app.UseMvc();
    }

    public static void Seed(Context context)
    {
      Game game1 = new Game { };
      Game game2 = new Game { };

      context.Games.Add(game1);
      context.Games.Add(game2);

      Collection collection1 = new Collection { };

      context.Add(collection1);

      Game_Mtm_Collection game_Mtm_Collection1 = new Game_Mtm_Collection
      {
        GameId = game1.Id,
        Game = game1,
        CollectionId = collection1.Id,
        Collection = collection1
      };

      // todo Uncomment to see the issue.
      //context.Add(game_Mtm_Collection1);
      context.SaveChanges();
    }

  }
}
