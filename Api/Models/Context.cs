﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Api.Models
{
  public sealed class Context : DbContext
  {
    public DbSet<Game> Games { get; set; }
    public DbSet<Collection> Collections { get; set; }
    public DbSet<Game_Mtm_Collection> Game_Mtm_Collections { get; set; }

    public Context() { }

    public Context(DbContextOptions<Context> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);

      EntityTypeBuilder<Game_Mtm_Collection> game_Mtm_Collection = builder.Entity<Game_Mtm_Collection>();
      game_Mtm_Collection.HasKey(_ => new { _.GameId, _.CollectionId });
      game_Mtm_Collection.HasOne(_ => _.Game)
        .WithMany(_ => _.Game_Mtm_Collections)
        .HasForeignKey(_ => _.GameId);
      //game_Mtm_Collection.HasOne(_ => _.Collection)
      //  .WithMany(_ => _.Game_Mtm_Collections)
      //  .HasForeignKey(_ => _.CollectionId);
    }
  }
}
