﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
  public class Collection
  {
    public Guid Id { get; set; }
    public List<Game_Mtm_Collection> Game_Mtm_Collections { get; set; }
  }
}