﻿using System.Collections.Generic;
using Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
  [Route("api/collections")]
  [ApiController]
  public class CollectionsController : ControllerBase
  {
    private Context Context { get; }

    public CollectionsController(Context context)
    {
      Context = context;
    }

    [HttpGet]
    public ActionResult<IEnumerable<Game>> Get()
    {
      return Ok(Context.Collections);
    }
  }
}