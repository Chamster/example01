﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{
  [Route("api/games")]
  [ApiController]
  public class GamesController : ControllerBase
  {
    private Context Context { get; }

    public GamesController(Context context)
    {
      Context = context;
    }

    [HttpGet]
    public ActionResult<IEnumerable<Game>> Get()

    {
      return Ok(Context.Games.Include(_ => _.Game_Mtm_Collections));
    }

    [HttpGet("{id}")]
    public ActionResult<Game> Get(Guid id)
    {
      return Ok(Context.Games.SingleOrDefault(_ => _.Id == id));
    }
  }
}